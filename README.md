README
======

Introduction
------------

This directory contains the source code of the Pipeline Mapper (PIPER), the main plugin to initiaze a pipeline developed with the POPPY framework.

See "POPPY User Manual" for more details.

License
-------
POPPY is under GPL license.

Acknowledgement
-----------
POPPY is project developed by the RPW Operations Centre (ROC) team based at LESIA (Meudon, France).
The ROC is funded by the Centre National d'Etudes Spatiale (CNES) in the framework of the European Space Agency (ESA) Solar Orbiter mission.

Contact
-------
xavier.bonnin@obspm.fr (project manager)
sonny.lion@obspm.fr (software designer)
quynh-nhu.nguyen@obspm.fr (software developer)