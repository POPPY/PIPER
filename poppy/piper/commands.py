#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from poppy.core.command import Command
from poppy.piper.tasks import push_descriptor, \
    list_plugins, reset_database, delete_database

__all__ = []


class PiperCommands(Command):
    """
    Manage the commands relative to the PIPER module.
    """
    __command__ = "piper"
    __command_name__ = "piper"
    __parent__ = "master"
    __parent_arguments__ = ["base"]
    __help__ = """
        Commands relative to the PIPER module, responsible for the
        initialization and the setup of the static information in the
        database.
    """


class LoadDescriptor(Command):
    """
    Command to load the plugin descriptor files and set the parameters into the
    configuration file.
    """
    __command__ = "descriptor"
    __command_name__ = "descriptor"
    __parent__ = "piper"
    __parent_arguments__ = ["base"]
    __help__ = "Command to load the plugin descriptor files" \
               " into the database"

    def add_arguments(self, parser):
        """
        Add arguments to the descriptor loading command.
        """
        # To force database content update if the descriptor content has changed
        parser.add_argument(
            "--force",
            action='store_true',
            help="Force the update of the database content",
        )

        parser.add_argument(
            "--reset",
            action='store_true',
            help="Reset the database content before loading descriptor(s)",
        )

    def setup_tasks(self, pipeline):
        """
        Execute what is necessary to do the command of reading and setting the
        parameters from the descriptor file to the POPPy database.
        """

        # the task
        task = push_descriptor()

        # set the pipeline for this situation
        pipeline | task
        pipeline.start = task

class ListPlugins(Command):
    """
    Command to get the list of loaded plugin(s) in the database.
    """
    __command__ = "list_plugins"
    __command_name__ = "list_plugins"
    __parent__ = "piper"
    __parent_arguments__ = ["base"]
    __help__ = "Command to list the plugin(s) loaded in the database"

    def setup_tasks(self, pipeline):
        """
        Call ListPlugin command

        :param self:
        :param args:
        :return:
        """

        # task
        start = list_plugins()
        pipeline | start
        pipeline.start = start



class ResetDatabase(Command):
    """
    Command to reset poppy database (i.e., poppy schema).
    """
    __command__ = "reset_database"
    __command_name__ = "reset_database"
    __parent__ = "piper"
    __parent_arguments__ = ["base"]
    __help__ = "Command to reset poppy database (i.e., poppy schema)"

    def add_arguments(self, parser):
        """
        Add arguments to the reset_database command.
        """

        # To force database resetting
        parser.add_argument(
            "--force",
            action='store_true',
            help="Force the reset of the database content",
        )

    def setup_tasks(self, pipeline):

        start = reset_database()

        pipeline | start
        pipeline.start = start


class DeleteDatabase(Command):
    """
    Command to delete poppy schema in the database.
    """
    __command__ = "delete_database"
    __command_name__ = "delete_database"
    __parent__ = "piper"
    __parent_arguments__ = ["base"]
    __help__ = "Command to delete poppy schema in the database"

    def add_arguments(self, parser):
        """
        Add arguments to the delete_database command.
        """

        # To force database deleteing
        parser.add_argument(
            "--force",
            action='store_true',
            help="Force the delete of the POPPy schema",
        )

    def setup_tasks(self, pipeline):

        start = delete_database()

        pipeline | start
        pipeline.start = start