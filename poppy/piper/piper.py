#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Pipeline mapper (Piper) of POPPy
"""

import collections

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from sqlalchemy import update

from poppy.core.logger import logger
from poppy.core.db.connector import Connector
from poppy.core.configuration import Configuration
from poppy.core.db.handlers import get_or_create_with_info
from poppy.core.db.handlers import get_or_create
from poppy.core.db.handlers import create
try:
    from poppy.pop.models.pipeline import PipelineDescr
    from poppy.pop.models.comp_identifier import ComponentIdentifier
    from poppy.pop.models.release import ReleaseDescr
    from poppy.pop.models.database import DatabaseDescr
    from poppy.pop.models.plugin import PluginDescr
    from poppy.pop.models.plugin import TaskTargetMap
    from poppy.pop.models.target import TargetDescr
except:
    logger.warning('WarningLegacy: Cannot import models from poppy.pop! \n '
                   'Check POPPy version is compatible with current PIPER version.')

from poppy.pop.plugins import Plugin
from poppy.core.conf import settings

__all__ = ["Piper"]


RELEASE_FIELDS = [
    "contact",
    "institute",
    "reference",
    "url",
    "file",
]

RELEASE_CHECK_FIELDS = RELEASE_FIELDS + ["author", "modification"]


class PiperError(Exception):
    """
    Errors for PIPER module.
    """


class Piper(object):

    def __init__(self):
        # init some variable in internal
        self.initialize()

        # named tuple for output targets and their plugin
        self.plugin = collections.namedtuple("Plugin", ["target", "tasks"])

    def reset_db(self):
        """
        Reset Poppy database schema content.

        :return:
        """

        # Use poppy.pop first migration upgrade/downgrade
        from poppy.pop.models.versions.poppy_pop_0001_initial_ import upgrade, downgrade

        logger.info('Resetting poppy database...')
        downgrade()
        upgrade()
        logger.info('poppy database has been cleaned')

    def delete_db(self):

        # get a session to the POPPy database
        session = self.get_session()

        # get a session to the POPPy database
        cmd = 'DROP SCHEMA IF EXISTS poppy CASCADE'
        logger.info(cmd)
        try:
            rs = session.execute(cmd)
        except:
            logger.error(f'Command "{cmd}" has failed')

    def load_descriptor(self):
        """
        Load the data from the descriptor in the XML format into a python
        dictionary.
        """
        # read the descriptor in json format
        logger.info("Loading {0}...".format(Configuration.manager["descriptor"].filename))
        self.content = Configuration.manager["descriptor"]

    def push_descriptor(self, force=False):
        """
        To push the structure of the descriptor into the POPPy database in order
        to set the static data.
        """

        # Get --force input keyword value
        self.force = force

        # get a session to the POPPy database
        self.get_session()

        # load the pipeline information into the database
        self.load_pipeline_descr()

        # load the database information
        self.load_database_descr()

        # load the plugin description
        self.load_plugin_descr()

        # validate targets
        self.validate_targets()

        # put into the database
        self.validate()

    def get_session(self):
        # get the name of the database for the pipeline
        database = Connector.manager[settings.MAIN_DATABASE].get_database()

        # get the database object and check the connection
        database.connectDatabase()
        if not database.connected:
            logger.error("{0} is not connected".format(database))
            return

        # get a session object to put all the information
        self.session = database.scoped_session

    def validate(self):
        # commit all changes into the database
        self.session.commit()

    def initialize(self):
        """
        Init some internal variables for storage.
        """
        # init containers for plugins
        self.plugin_list = list()

    def validate_targets(self):
        """
        Validate the targets read in descriptors, to see if a definition is
        missing or not.
        """
        # get all targets defined in the plugin
        targets = set()
        for plugin, modes in self.plugin_list:
            for target in plugin.plugin_targets:
                if target.target_category == "Output":
                    # check that defined targets are unique
                    if target.target in targets:
                        logger.warning(
                            f"Target {target.target} is defined multiple times."
                        )
                    else:
                        targets.add(target.target)

        # loop over plugin registered in the database
        for plugin, tasks in self.plugin_list:
            # loop over tasks
            for task in tasks:
                # loop over the targets defined in inputs
                for target in task["inputs"].values():
                    # get the identifier
                    query = self.session.query(ComponentIdentifier).filter_by(
                        comp_identifier=target["identifier"],
                    )
                    identifier = query.one_or_none()

                    # check existing
                    if identifier is None:
                        logger.error(
                            (
                                "The input target {0} of plugin {1} is " +
                                "not defined"
                            ).format(
                                target["identifier"],
                                plugin,
                            )
                        )
                        continue

                    # get the target with the good version for the given
                    # pipeline
                    query = self.session.query(TargetDescr)
                    query = query.filter_by(
                        pipeline=self.pipeline,
                        poppy_id=identifier,
                    )

                    tg = query.one_or_none()

                    # check existing
                    if tg is None:
                        logger.error(
                            (
                                "The input target {0} of plugin {1} is " +
                                "not defined"
                            ).format(
                                target["identifier"],
                                plugin,
                            )
                        )
                        continue

                    # the target seems to be existing, check that it has been
                    # defined for the set of descriptors used for this version
                    # of the pipeline
                    if tg not in targets:
                        logger.warning(
                            (
                                "Input target {0} is defined but in an " +
                                "older set of descriptors"
                            ).format(target["identifier"])
                        )

                    # target is existing and defined in this set of
                    # descriptors, so add it to the database linked to the
                    # plugin
                    get_or_create(
                        self.session,
                        TaskTargetMap,
                        target=tg,
                        plugin=plugin,
                        target_category="Input",
                    )

    def load_pipeline_descr(self):
        """
        To load the information of the pipeline into the database.
        """
        # pipeline information into the descriptor
        self.pipeline = self._get_pipeline_descr(
            self.session,
            self.content["pipeline"],
        )

    def load_database_descr(self):
        """
        To load the description of the database into the POPPy database.
        """
        # loop over database information into the descriptor
        databases = self.content["pipeline.databases"]
        for database in databases:
            self._get_database_descr(self.session, self.pipeline, database)

    def load_plugin_descr(self):
        """
        To load the information of the plugin into the POPPy database.
        """

        plugins = settings.PLUGINS

        # loop over plugin information into the descriptor
        for plugin in plugins:
            logger.info("Loading {0}...".format(Plugin.manager[plugin].descriptor_path))
            # get the module
            plugin_desc = self._get_plugin_descr(
                self.session,
                self.pipeline,
                Plugin.manager[plugin].descriptor,
            )

            # load targets in tasks
            self._load_output_target_descr(
                self.session,
                self.pipeline,
                plugin_desc,
                Plugin.manager[plugin].descriptor["tasks"],
            )

            # add the software to register
            self.plugin_list.append(
                self.plugin(
                    plugin_desc,
                    Plugin.manager[plugin].descriptor["tasks"],
                )
            )

    def _load_output_target_descr(self, session, pipeline, plugin, tasks):
        """
        To load the information of the output targets into the POPPy database.
        """
        # loop over tasks and targets
        for task in tasks:
            # loop over output targets
            for target in task["outputs"].values():
                # insert defined target into the POPPy database
                tg = self._get_output_target_descr(session, pipeline, target)

                # create the map object with the category of the target
                get_or_create(
                    session,
                    TaskTargetMap,
                    target=tg,
                    plugin=plugin,
                    target_category="Output",
                )

    def _get_pipeline_descr(self, session, pipeline):
        """
        Transform a pipeline information from the descriptor into an object to
        put in the database.
        """
        # create the poppy identifier if not present, or get it
        poppy_id = self._get_identifier(session, pipeline)

        # join with release to see if release is already defined
        release = self._join_release(
            session,
            PipelineDescr,
            pipeline["release.version"],
            pipeline["release.date"],
            poppy_id=poppy_id,
        )

        # if the join returns nothing, create the release and create the
        # pipeline
        if release is None:
            # get release from the descriptor
            release = self._get_release_descr(session, pipeline["release"])

            # create the pipeline object and return
            return create(session, PipelineDescr, poppy_id=poppy_id, release=release)

        # else we need to check the release
        self._check_release(release[1], pipeline["release"])

        # if nothing different returns the old one
        return release[0]

    def _join_release(self, session, model, version, date, **kwargs):
        """
        Make a join with the to check if not modified in order to have not
        updated releases.
        """
        query = session.query(model, ReleaseDescr)
        query = query.filter_by(**kwargs)
        query = query.join(ReleaseDescr)
        query = query.filter_by(release_version=version)
        try:
            return query.one_or_none()
        except MultipleResultsFound:
            raise PiperError(
                (
                    "Found multiple results for release {0}, {1} and model {2}"
                ).format(version, date, model)
            )

    def _join_level(self, session, model, level, target_id, **kwargs):
        """
        Make a join with the to check if not modified in order to have not
        updated level.
        """
        query = session.query(model, ComponentIdentifier)
        query = query.filter_by(target_level=level)
        query = query.filter_by(**kwargs)
        query = query.join(ComponentIdentifier)
        query = query.filter_by(comp_identifier=target_id)
        #query = query.join(model)

        try:
            return query.one_or_none()
        except MultipleResultsFound:
            raise PiperError(
                (
                    'Found multiple results for target_id "{0}", level "{1}" and model "{2}"'
                ).format(target_id, level, model)
            )

    def _get_identifier(self, session, infos):
        """
        Get the identifier from the database or create it.
        """
        identifier, created = get_or_create_with_info(
            session,
            ComponentIdentifier,
            comp_identifier=infos["identifier"],
            create_method_kwargs=dict(
                comp_name=infos["name"],
                comp_description=infos["description"],
            ),
        )
        if not created:
            if identifier.comp_name != infos["name"]:
                if not self.force:
                    raise PiperError(
                        'The name attribute of "{0}" has changed from the'.format(
                            infos["identifier"]
                        ) +
                        " last definition\n" +
                        'In database: "{0}", in descriptor: "{1}"'.format(
                            identifier.comp_name,
                            infos["name"],
                        )
                    )
                else:
                    # Else if --force input keyword is defined,
                    # then force database content updating
                    update(ComponentIdentifier).where(
                        ComponentIdentifier.comp_identifier == infos["identifier"]
                                                      ).values(
                        comp_name = infos["name"])
                    session.commit()


            # Same for description
            if identifier.comp_description != infos["description"]:
                if not self.force:
                    raise PiperError(
                        'The description attribute of "{0}" has changed '.format(
                            infos["identifier"]
                        ) + "from the last definition\n" +
                        'In database: "{0}", in descriptor: "{1}"'.format(
                            identifier.comp_description,
                            infos["description"],
                        )
                    )
                else:
                    # Else if --force input keyword is defined,
                    # then force database content updating
                    logger.warning(
                        'The description attribute of "{0}" has changed '.format(
                            infos["identifier"]
                        ) + "from the last definition\n" +
                        'In database: "{0}", in descriptor "{1}"'.format(
                            identifier.comp_description,
                            infos["description"],
                        ))
                    update(ComponentIdentifier).where(
                        ComponentIdentifier.comp_identifier == infos["identifier"]
                                                      ).values(
                        comp_description = infos["description"])
                    session.commit()
        else:
            identifier.comp_name = infos["name"]
            identifier.comp_description = infos["description"]

        return identifier

    def _get_database_descr(self, session, pipeline, database):
        """
        Transform a database information from the descriptor into an object to
        put in the database.
        """
        # create the poppy identifier if not present, or get it
        poppy_id = self._get_identifier(session, database)

        # join with release to see if release is already defined
        release = self._join_release(
            session,
            DatabaseDescr,
            database["release.version"],
            database["release.date"],
            pipeline=pipeline,
            poppy_id=poppy_id,
        )

        # if the join returns nothing, create the release and create the
        # database
        if release is None:
            # get release from the descriptor
            release = self._get_release_descr(session, database["release"])

            # create the pipeline object and return
            return create(
                session,
                DatabaseDescr,
                poppy_id=poppy_id,
                pipeline=pipeline,
                release=release,
            )

        # else we need to check the release
        self._check_release(release[1], database["release"])

        # if nothing different returns the old one
        return release[0]

    def _get_plugin_descr(self, session, pipeline, plugin):
        """
        Transform a plugin information from the descriptor into an object to
        put in the database.
        """
        # create the poppy identifier if not present, or get it
        poppy_id = self._get_identifier(session, plugin["identification"])

        # join with release to see if release is already defined
        release = self._join_release(
            session,
            PluginDescr,
            plugin["release.version"],
            plugin["release.date"],
            pipeline=pipeline,
            poppy_id=poppy_id,
        )

        # if the join returns nothing, create the release and create the
        # plugin
        if release is None:
            # get release from the descriptor
            release = self._get_release_descr(session, plugin["release"])

            # create the pipeline object and return
            return create(
                session,
                PluginDescr,
                poppy_id=poppy_id,
                pipeline=pipeline,
                release=release,
            )

        # else we need to check the release
        self._check_release(release[1], plugin["release"])

        # if nothing different returns the old one
        return release[0]

    @staticmethod
    def query_plugins(session):
        """
        Query the list of plugins from a given database session

        :return: list of plugins found
        """

        try:
            plugin_ids = [result[0] for result in
                          session.query(PluginDescr.comp_identifier_id).all()]
        except NoResultFound:
            logger.warning("No plugin description loaded in the database.")
            return []
        else:
            query = session.query(ComponentIdentifier, PluginDescr, ReleaseDescr)
            query = query.filter(ComponentIdentifier.id_comp_identifier.in_(plugin_ids))
            query = query.join(PluginDescr) # required to join ComponentIdentifier and ReleaseDescr tables
            query = query.join(ReleaseDescr)

        return query.all()


    def get_plugins(self):
        """
        Get the list of plugins loaded in the database

        :return: list of plugins found
        """

        # get a session to the POPPy database
        self.get_session()

        return self.query_plugins(self.session)



    def _get_output_target_descr(self, session, pipeline, target):
        """
        Transform a target information from the descriptor into an object to
        put in the database.
        """
        # create the poppy identifier if not present, or get it
        poppy_id = self._get_identifier(session, target)

        # join with component_identifier to see if (comp_identifier,
        # level) is already defined
        target_descr = self._join_level(
            session,
            TargetDescr,
            target["level"],
            target["identifier"],
            pipeline=pipeline,
            poppy_id=poppy_id,
        )

        # if the join returns nothing, create the target_descr and create the
        # target
        if target_descr is None:

            # create the pipeline object and return
            return create(
                session,
                TargetDescr,
                poppy_id=poppy_id,
                pipeline=pipeline,
                target_level=target["level"],
            )

        # if nothing different returns the old one
        return target_descr[0]

    def _get_release_descr(self, session, release):
        """
        Transform a release from the descriptor file into a release info to put
        inside the database.

        :param session: database session
        :param release: release to get or create
        :return:
        """

        # get the release or create it
        data, created = get_or_create_with_info(
            session,
            ReleaseDescr,
            release_version=release["version"],
            release_date=release["date"],
            release_author=release["author"],
            release_modification=release["modification"],
            )

        # add fields into the database
        for field in RELEASE_FIELDS:
            if field in release:
                setattr(data, "release_" + field, release[field])

        return data


    def get_latest_release(self, plugin_id):
        """
        Get the latest release info for a given plugin in the database.

        :param plugin_id: identifier of the plugin
        :return: release info
        """

        query = self.session.query(ReleaseDescr.release_version)
        query = query.join(PluginDescr) # required to join ComponentIdentifier and ReleaseDescr tables
        query = query.join(ComponentIdentifier)
        query = query.filter_by(comp_identifier=plugin_id)

        try:
            results = query.all()
            if not results:
                raise NoResultFound
        except NoResultFound:
            logger.warning(
                    "No release info found for plugin {0}!".format(
                        plugin_id))
            return None
        except MultipleResultsFound:
            raise PiperError(
                (
                    "Plugin {0} has more than one entry!"
                ).format(plugin_id)
            )
        else:

            latest_release = '-1'
            for entry in results:
                latest_release = max(latest_release, entry[0])

            if latest_release == '-1':
                logger.error("Release versions for plugin {0} are badly formatted!".format(
                    plugin_id
                ))
                return None
            else:
                return latest_release

    def _check_release(self, data, release):
        # loop over fields and check if they are present and not different
        for field in RELEASE_CHECK_FIELDS:
            # get the field or None
            if field in release:
                compared = release[field]
            else:
                compared = None

            # check it is the same as already defined
            value = getattr(data, "release_" + field)
            if value != compared:
                msg = ('"{0}" do not have the same value for "{1}"!' +
                            '\nIn database: "{2}", in descriptor: "{3}"'
                        ).format(data, field, value, compared)
                if not self.force:
                    raise PiperError(msg)
                else:
                    logger.warning(msg)
                    logger.warning("Old value will be replaced!")
                    setattr(data, "release_" + field, compared)


        # if the date has changed, update it in the database
        data.release_date = release["date"]

    @staticmethod
    def is_present(session, model, **kwargs):
        """
        Return True if an object with specified attributes is already present
        in the database or not.
        """
        # query the database and count similar objects
        try:
            return session.query(model).filter_by(**kwargs).first() > 0
        except:
            return False
