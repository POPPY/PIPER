#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from poppy.core.conf import settings
from poppy.core.db.dry_runner import DryRunner
from poppy.core.db.connector import Connector
from poppy.core.task import Task

from poppy.piper.tools import check_version
from poppy.piper import Piper

__all__ = ["push_descriptor",
           "list_plugins",
           "PiperTask",
           "reset_database",
           "delete_database"]

# TODO - Understand why the Task does not work
# get the task
#Task = Plugin.manager['poppy.piper'].task("insert_descriptors")


class PiperTask():
    """
    Base class for the task relative to the PIPER module. Class to manage
    actions to be done for setting the static information, descriptions and
    parameters into the database.
    """
    def __init__(self):
        # init forcing some parameters
        super(PiperTask, self).__init__()

        # force the dry run
        DryRunner().activate()


@Task.as_task(plugin_name='poppy.piper', name='push_descriptor')
@Connector.if_connected(settings.MAIN_DATABASE)
def push_descriptor(task):
    """
    Put the content of the descriptor files into the database.

    :param force: Force database upgrade (even if there is an inconsistency with descriptor content)
    :param reset: Reset database before loading descriptor(s)
    :return:
    """
    # create piper object handling low level
    DryRunner().activate()

    # Check version compatibility
    if not check_version():
        sys.exit()

    piper = Piper()

    # load the descriptor
    piper.load_descriptor()

    # if reset keyword is True, then clean database content first
    if task.pipeline.args.reset == True:
        reset_database()

    # push the descriptor
    piper.push_descriptor(force=task.pipeline.args.force)


@Task.as_task(plugin_name='poppy.piper', name='list_plugins')
@Connector.if_connected(settings.MAIN_DATABASE)
def list_plugins():
    """
    Print the list of plugin(s) loaded in the database.
    """
    # create piper object handling low level
    DryRunner().activate()

    # Check version compatibility
    if not check_version():
        sys.exit()

    # Initialize Piper object
    piper = Piper()

    # Get list of plugins
    plugins = piper.get_plugins()
    for plugin in plugins:
        print('"{0}" plugin loaded with info (version={1}, date={2}, author={3})'.format(
            plugin.PluginDescr.identifier,
            plugin.ReleaseDescr.release_version,
            plugin.ReleaseDescr.release_date,
            plugin.ReleaseDescr.release_author,
        ))


@Task.as_task(plugin_name='poppy.piper', name='reset_database')
@Connector.if_connected(settings.MAIN_DATABASE)
def reset_database(task):
    """
    Clean content of the POPPy database (i.e., poppy schema).

    :return:
    """

    # By default, ask to the user before cleaning
    if task.pipeline.args.force == False:
        answer = input("Are you sure? [YES]: ")
        if answer != 'YES':
            return

    # create piper object handling low level
    DryRunner().activate()

    # Check version compatibility
    if not check_version():
        sys.exit()

    # Initialize Piper object
    piper = Piper()

    # Run method to reset poppy database
    piper.reset_db()

@Task.as_task(plugin_name='poppy.piper', name='delete_database')
@Connector.if_connected(settings.MAIN_DATABASE)
def delete_database(task):
    """
    Delete the POPPy database schema.

    :return:
    """

    # By default, ask to the user before cleaning
    if task.pipeline.args.force == False:
        answer = input("Are you sure? [YES]: ")
        if answer != 'YES':
            return

    # create piper object handling low level
    DryRunner().activate()

    # Check version compatibility
    if not check_version():
        sys.exit()

    # Initialize Piper object
    piper = Piper()

    # Run method to delete database schema
    piper.delete_db()

