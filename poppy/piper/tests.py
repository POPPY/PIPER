from unittest import TestCase

import pytest
import unittest.mock as mock
from poppy.core.configuration import Configuration
from poppy.core.test import CommandTestCase
from poppy.piper.piper import Piper
import types

class PiperTests(TestCase):

    def setUp(self):
        self.piper = Piper()

    def test_load_descriptor(self):
        # mock the configuration class
        descriptor_configuration_value = types.SimpleNamespace(filename='my_descriptor_configuration_file')

        with mock.patch.object(Configuration, 'manager', {'descriptor': descriptor_configuration_value}):
            # load the fake descriptor
            self.piper.load_descriptor()

        assert self.piper.content == descriptor_configuration_value

    @pytest.mark.skip('Empty')
    def test_push_descriptor(self):
        pass

    @pytest.mark.skip('Empty')
    def test_get_session(self):
        pass

    @pytest.mark.skip('Empty')
    def test_validate(self):
        pass

    def test_initialize(self):
        self.piper.initialize()
        assert self.piper.plugin_list == list()

    @pytest.mark.skip('Empty')
    def test_validate_datasets(self):
        pass

    @pytest.mark.skip('Empty')
    def test_load_pipeline_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test_load_database_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test_load_software_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__load_output_ds_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__get_pip_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__join_release(self):
        pass

    @pytest.mark.skip('Empty')
    def test__get_identifier(self):
        pass

    @pytest.mark.skip('Empty')
    def test__get_db_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__get_sw_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__get_output_ds_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__get_release_info(self):
        pass

    @pytest.mark.skip('Empty')
    def test__check_release(self):
        pass


class TestPiperCommands(CommandTestCase):

    # run the test twice to verify the database rollback
    @pytest.mark.parametrize('execution_number', range(2))
    def test_load_descriptor(self, execution_number):
        from poppy.core.conf import Settings

        # define the expected plugin list
        expected_plugin_list = ['poppy.piper', 'poppy.pop']

        # create the commands
        command = ['pop', "piper", "descriptor"]
        db_upgrade = ['pop', "db", "upgrade", "heads"]

        # force the value of the plugin list
        with mock.patch.object(Settings, 'configure',
                               autospec=True,
                               side_effect=self.mock_configure_settings(dictionary={'PLUGINS': expected_plugin_list})):

            # apply database migrations
            self.run_command(db_upgrade)

            # check if the some descriptor has already been loaded
            plugin_list = [plugin.PluginDescr.identifier for plugin in Piper.query_plugins(self.session)]

            assert sorted(plugin_list) == []

            # run the command
            self.run_command(command)

        # query the new plugin list
        plugin_list = [plugin.PluginDescr.identifier for plugin in Piper.query_plugins(self.session)]

        # make assertions
        assert sorted(plugin_list) == expected_plugin_list
