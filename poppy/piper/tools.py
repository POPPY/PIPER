#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Tools for POPPY.PIPER plugin."""

from poppy.core.plugin import Plugin
from poppy.core.logger import logger


def get_plugin_version(plugin):
    """Get plugin version from its descriptor."""
    descriptor = Plugin.manager[plugin].descriptor
    return descriptor["release.version"]


def check_version():
    """
    Check poppycore against piper version (legacy)

    :return: True if current version of PIPER can be used, False otherwise
    """

    poppycore_version = get_plugin_version('poppy.core')
    piper_version = get_plugin_version('poppy.piper')
    if poppycore_version > '0.5.0':
        logger.warning(f'Current PIPER version {piper_version} is not compatible '
                       f'with current POPPyCore version {poppycore_version}! \n'
                       'Use POPPyCore 0.5.0')
        return False
    else:
        return True