#! /usr/bin/env python3
# -*- coding:Utf8 -*-

# -----------------------------------------------------------------------------
# All necessary import:
# -----------------------------------------------------------------------------
import json
import os

from setuptools import find_packages
from setuptools import setup

ROOT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

REQ_FILE = os.path.join(ROOT_DIRECTORY, "requirements.txt")


def get_reqs(req_file):
    """Get module dependencies from requirements.txt."""
    if not os.path.isfile(req_file):
        raise BaseException("No requirements.txt file found, aborting!")
    else:
        with open(req_file, 'r') as fr:
            requirements = fr.read().splitlines()

    return requirements


def _get_version():
    """Get version from plugin descriptor"""
    with open("poppy/piper/descriptor.json", 'r') as f:
        desc = json.load(f)
    return desc["release"]["version"]


def _get_description():
    """Get description from plugin descriptor"""
    with open("poppy/piper/descriptor.json", 'r') as f:
        desc = json.load(f)
    return desc["identification"]["description"]


setup(
    name='poppy.piper',
    version=_get_version(),
    description=_get_description(),
    author="ROC team",
    license="GNU GPLv3",
    packages=find_packages(),
    install_requires=get_reqs(REQ_FILE),
    include_package_data=True,
    zip_safe=False,
    package_data={
        "": ["*.json", "*.xlsx", "*.so", "*.cdf", "*.sql", "*.dat", "*.html"]
    }
)
